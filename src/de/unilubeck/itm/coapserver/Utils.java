package de.unilubeck.itm.coapserver;

import android.content.Context;
import android.provider.Settings.Secure;
import com.google.common.collect.HashBiMap;
import de.uniluebeck.itm.spitfire.nCoap.message.CoapRequest;
import de.uniluebeck.itm.spitfire.nCoap.message.options.Option;
import de.uniluebeck.itm.spitfire.nCoap.message.options.OptionRegistry.MediaType;
import org.apache.http.conn.util.InetAddressUtils;

import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.*;

public class Utils {

    /**
     * Comma separated List of IP adresses of available network interfaces
     * @param useIPv4
     * @return
     */
    public static String getIPAddress(boolean useIPv4) {

        String addresses = "";
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress().toUpperCase();
                        boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        if (useIPv4) {
                            if (isIPv4)
                                addresses += sAddr + ", ";
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 port suffix
                                if(delim<0) addresses += sAddr + ", ";
                                else addresses += sAddr.substring(0, delim) + ", ";
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) { } // for now eat exceptions
        if(addresses == null || addresses.length() <= 3) return "";
        return addresses.subSequence(0, addresses.length()-2).toString();
    }

    /**
     * Absolute URI of this request. For use as namespace for RDF XML responses
     * @param request CoAP request
     * @return absolute uri
     */
    public static String targetUri(CoapRequest request) {
        String path = request.getTargetUri().getPath();
        String[] ipAddresses = Utils.getIPAddress(true).split(", ");
        if(ipAddresses.length > 0 && !ipAddresses[0].isEmpty()) {
            return "coap://"+ipAddresses[0] + path;
        }
        else return "coap://example.com"+path;
    }

    /**
     * Parse x-www-form-urlencoded parameters from request query
     * @param options Options List from CoAP request
     * @return parsed Parameters
     */
    public static Map<String, String> parseParams(List<Option> options) {
        Map<String, String> result = new HashMap<String, String>();
        for(Option o : options) {
            String param = o.getDecodedValue().toString();
            int i = param.indexOf('=');
            try {
                String key = param.substring(0,i).toLowerCase();
                String value = URLDecoder.decode(param.substring(i+1), "utf8");
                result.put(key, value);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    /**
     * Returns a unique device id, which is stable across restarts
     * @param context
     * @return
     */
    public static String deviceId(Context context) {
        return Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
    }

    private static TimeZone tz = TimeZone.getTimeZone("UTC");
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
    {
        simpleDateFormat.setTimeZone(tz);
    }
    public static String toISODate(Date date) {
        if(date == null) return "";
        return simpleDateFormat.format(date);
    }


    public static final HashBiMap<String, MediaType> MEDIA_TYPE_MAP = HashBiMap.create();
    static {
        MEDIA_TYPE_MAP.put("RDF/XML", MediaType.APP_RDF_XML);
        MEDIA_TYPE_MAP.put("TURTLE", MediaType.APP_TURTLE);
        MEDIA_TYPE_MAP.put("N3", MediaType.APP_N3);
    }

}
