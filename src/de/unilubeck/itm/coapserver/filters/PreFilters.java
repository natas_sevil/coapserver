package de.unilubeck.itm.coapserver.filters;

import java.lang.annotation.*;

@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface PreFilters {
    public Class<? extends PreFilter>[] value();
}
