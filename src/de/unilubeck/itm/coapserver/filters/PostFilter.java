package de.unilubeck.itm.coapserver.filters;

import de.uniluebeck.itm.spitfire.nCoap.message.CoapRequest;
import de.uniluebeck.itm.spitfire.nCoap.message.CoapResponse;

public interface PostFilter {
    public void postProcess(CoapRequest request, CoapResponse response) throws Exception;
}
