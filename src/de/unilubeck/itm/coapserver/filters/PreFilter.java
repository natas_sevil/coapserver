package de.unilubeck.itm.coapserver.filters;

import de.uniluebeck.itm.spitfire.nCoap.message.CoapRequest;


public interface PreFilter {
    public void preProcess(CoapRequest request) throws Exception;
}
