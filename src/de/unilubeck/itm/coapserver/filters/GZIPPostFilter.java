package de.unilubeck.itm.coapserver.filters;

import de.unilubeck.itm.coapserver.Utils;
import de.uniluebeck.itm.spitfire.nCoap.message.CoapRequest;
import de.uniluebeck.itm.spitfire.nCoap.message.CoapResponse;
import de.uniluebeck.itm.spitfire.nCoap.message.options.OptionRegistry.MediaType;
import de.uniluebeck.itm.spitfire.nCoap.message.options.OptionRegistry.OptionName;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

public class GZIPPostFilter implements PostFilter {


    public static final String ACCEPT_ENCODING = "accept-encoding";

    @Override
    public void postProcess(CoapRequest request, CoapResponse response) throws Exception {
        Map<String,String> params = Utils.parseParams(request.getOption(OptionName.URI_QUERY));
        if(params.containsKey(ACCEPT_ENCODING) && params.get(ACCEPT_ENCODING).equalsIgnoreCase("gzip")) {
            int len = response.getPayload().array().length;
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(len);
            GZIPOutputStream gzipOutputStream = new GZIPOutputStream(byteArrayOutputStream, len);
            gzipOutputStream.write(response.getPayload().array());
            gzipOutputStream.close();
            response.setPayload(byteArrayOutputStream.toByteArray());
            response.setContentType(MediaType.APP_OCTET_STREAM);
        }
    }
}
