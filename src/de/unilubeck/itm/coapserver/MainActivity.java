package de.unilubeck.itm.coapserver;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import android.widget.CompoundButton.OnCheckedChangeListener;
import de.unilubeck.itm.coapserver.R.id;
import de.unilubeck.itm.coapserver.R.layout;
import de.unilubeck.itm.coapserver.model.*;
import de.unilubeck.itm.coapserver.services.*;
import de.uniluebeck.itm.spitfire.nCoap.application.webservice.WebService;

import java.net.URI;
import java.util.LinkedList;
import java.util.List;

public class MainActivity extends Activity {
    public static final int DIALOG_SSP = 1;
    private CoapServer server;
    private List<ToggleButton> serviceButtons = new LinkedList<ToggleButton>();
    private List<WebService> services = new LinkedList<WebService>();
    private LinearLayout mainLayout;

    private ToggleButton serverTbtn;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);



        mainLayout = (LinearLayout) findViewById(id.mainLayout);

        TextView ipTxt = (TextView) findViewById(id.ipTxt);
        ipTxt.setText(Utils.getIPAddress(true));

        serverTbtn = (ToggleButton) findViewById(id.serverTBtn);
        serverTbtn.setEnabled(true);
        serverTbtn.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    enableServer();
                    enableAllServiceButtons();
                } else {
                    disableServer();
                }
            }
        });

        enableServer();

        String deviceId = Utils.deviceId(this);

        bindService(new TestNotObservableWebService("/test"), "Test Service:", true);

        TimeModel timeModel = new TimeModel(deviceId);
        bindService(new TimeService("/time", timeModel), "Time Service:", false);

        LocationModel locationModel = new LocationModel(deviceId);
        bindService(new LocationService("/location", this, locationModel), "Location Service:",true);

        TemperatureSensorModel temperatureSensorModel = new TemperatureSensorModel(deviceId);
        bindService(new SensorService("/sensors/temperature", this, Sensor.TYPE_TEMPERATURE, SensorManager.SENSOR_DELAY_NORMAL,temperatureSensorModel), "Temperature Service:", false);

        OrientationSensorModel orientationSensorModel = new OrientationSensorModel(deviceId);
        bindService(new SensorService("/sensors/orientation", this, Sensor.TYPE_ORIENTATION, SensorManager.SENSOR_DELAY_NORMAL,orientationSensorModel), "Orientation Service:",true);

        AccelerationSensorModel accelerometerModel = new AccelerationSensorModel(deviceId);
        bindService(new SensorService("/sensors/accelerometer", this, Sensor.TYPE_ACCELEROMETER, SensorManager.SENSOR_DELAY_NORMAL,accelerometerModel), "Accelerometer Service:", false);

        GyroscopeSensorModel gyroscopeSensorModel = new GyroscopeSensorModel(deviceId);
        bindService(new SensorService("/sensors/gyroscope", this, Sensor.TYPE_GYROSCOPE, SensorManager.SENSOR_DELAY_NORMAL,gyroscopeSensorModel), "Gyroscope Service:",true);

        LightSensorModel lightSensorModel = new LightSensorModel(deviceId);
        bindService(new SensorService("/sensors/light", this, Sensor.TYPE_LIGHT, SensorManager.SENSOR_DELAY_NORMAL,lightSensorModel), "Light Service:", false);

        MagneticFieldSensorModel magneticFieldSensorModel = new MagneticFieldSensorModel(deviceId);
        bindService(new SensorService("/sensors/magnetic_field", this, Sensor.TYPE_MAGNETIC_FIELD, SensorManager.SENSOR_DELAY_NORMAL,magneticFieldSensorModel), "Magnetic Field Service:",true);

        PressureSensorModel pressureSensorModel = new PressureSensorModel(deviceId);
        bindService(new SensorService("/sensors/pressure", this, Sensor.TYPE_PRESSURE, SensorManager.SENSOR_DELAY_NORMAL,pressureSensorModel), "Pressure Service:", false);

        ProximitySensorModel proximitySensorModel = new ProximitySensorModel(deviceId);
        bindService(new SensorService("/sensors/proximity", this, Sensor.TYPE_PROXIMITY, SensorManager.SENSOR_DELAY_NORMAL,proximitySensorModel), "Proximity Service:",true);
// Disable for Maven

        GravitySensorModel gravitySensorModel = new GravitySensorModel(deviceId);
        bindService(new SensorService("/sensors/gravity", this, Sensor.TYPE_GRAVITY, SensorManager.SENSOR_DELAY_NORMAL,gravitySensorModel), "Gravity Service:", false);

        LinearAccelerationSensorModel linearAccelerationSensorModel = new LinearAccelerationSensorModel(deviceId);
        bindService(new SensorService("/sensors/linear_acceleration", this, Sensor.TYPE_LINEAR_ACCELERATION, SensorManager.SENSOR_DELAY_NORMAL,linearAccelerationSensorModel), "Linear Acceleration Service:",true);

        RelativeHumiditySensorModel relativeHumiditySensorModel = new RelativeHumiditySensorModel(deviceId);
        bindService(new SensorService("/sensors/relative_humidity", this, Sensor.TYPE_RELATIVE_HUMIDITY, SensorManager.SENSOR_DELAY_NORMAL,relativeHumiditySensorModel), "Relative Humidity Service:", false);

        RotationVectorSensorModel rotationVectorSensorModel = new RotationVectorSensorModel(deviceId);
        bindService(new SensorService("/sensors/rotation_vector", this, Sensor.TYPE_ROTATION_VECTOR, SensorManager.SENSOR_DELAY_NORMAL,rotationVectorSensorModel), "Rotation Vector Service:",true);
// Disable for Maven

        enableAllServiceButtons();
    }

    private void bindService(final WebService service, String label, boolean oddRow) {
        services.add(service);

        LinearLayout layout = (LinearLayout) getLayoutInflater().inflate(R.layout.service_row, null);
        TextView labelTv = (TextView) layout.findViewById(id.serviceLabelTv);
        labelTv.setText(label);

        ToggleButton toggleButton = (ToggleButton) layout.findViewById(id.serviceTBtn);
        serviceButtons.add(toggleButton);
        toggleButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                server.registerService(service);
            }
            else {
                server.removeService(service.getPath());
            }
            }
        });
        toggleButton.setChecked(false);

        if(oddRow) {
            layout.setBackgroundColor(Color.parseColor("#b5c2df"));
        }
        mainLayout.addView(layout);
    }

    private void enableServer() {
        server = new CoapServer();
        enableAllServices();
    }

    private void disableServer() {
        disableAllServiceButtons();
        server.shutdown();
        server = null;
    }

    private void enableAllServiceButtons() {
        for(ToggleButton t:serviceButtons) {
            t.setEnabled(true);
            t.setChecked(true);
        }
    }

    private void disableAllServiceButtons() {
        for(ToggleButton t:serviceButtons) {
            t.setChecked(false);
            t.setEnabled(false);
        }
    }

    private void enableAllServices() {
        for(WebService service : services) {
            server.removeService(service.getPath());
        }

        for(WebService service : services) {
            server.registerService(service);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disableServer();
    }

    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case id.action_settings:
                showDialog(DIALOG_SSP);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public Dialog onCreateDialog(int id) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final View dialogLayout = getLayoutInflater().inflate(layout.ssp_dialog, null);

        builder.setTitle("SSP-Konfiguration");
        builder.setView(dialogLayout);
        builder.setPositiveButton("OK", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                EditText sspUriTxt = (EditText) dialogLayout.findViewById(R.id.sspUriTxt);
                server.setSspURI(URI.create(sspUriTxt.getText().toString()));
                new AsyncTask<Void, Void, Void>() {
                    private Exception e;

                    @Override
                    protected Void doInBackground(Void... params) {
                        try {
                            server.sendSSPRegistration();
                        } catch (Exception e) {
                            e.printStackTrace();
                            this.e = e;
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        if( e!= null) {
                            Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                }.execute();
            }
        });

        builder.setNegativeButton("Abbrechen", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        final Dialog d = builder.create();

        return d;
    }
}
