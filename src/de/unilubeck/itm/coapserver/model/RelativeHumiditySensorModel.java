package de.unilubeck.itm.coapserver.model;

import de.unilubeck.itm.coapserver.Utils;
import de.unilubeck.itm.coapserver.serialization.*;

import java.util.Arrays;

@RDFDatatype
@RDFDatatypeProperties({@RDFDatatypeProperty(property = RelativeHumiditySensorModel.VAL_TYPE, ns= RelativeHumiditySensorModel.NS_TYPE, name="type"),
                        @RDFDatatypeProperty(property = RelativeHumiditySensorModel.VAL_OBSERVED, ns= RelativeHumiditySensorModel.NS_OBSERVED, name="observedProperty"),
                        @RDFDatatypeProperty(property = RelativeHumiditySensorModel.VAL_UOM, ns= RelativeHumiditySensorModel.NS_UOM, name="uomInUse")})
public class RelativeHumiditySensorModel extends SensorModel {

    public static final String PREFIX_ABOUT = "http://spitfire-project.eu/sensor/";

    public static final String NS_TYPE = "http://www.w3.org/2000/01/rdf-schema#";
    public static final String VAL_TYPE = "http://purl.oclc.org/NET/ssnx/ssn#Sensor";

    public static final String NS_OBSERVED = "http://purl.oclc.org/NET/ssnx/ssn#";
    public static final String VAL_OBSERVED = "http://purl.oclc.org/NET/muo/ucum/physical-quality/relativeHumidity";

    public static final String NS_UOM = "http://spitfire-project.eu/cc/spitfireCC_n3.owl#";
    public static final String VAL_UOM = "http://spitfire-project.eu/uom/percent";


    public RelativeHumiditySensorModel(String ressource_id) {
        super(ressource_id);
    }

    @RDFId
    public String getId() {
        return PREFIX_ABOUT + ressource_id;
    }


    @RDFObjectProperty(ns="http://www.loa-cnr.it/ontologies/DUL.owl#", name="hasValue")
    public String getHasValue() {
        return Arrays.toString(data);
    }

    @RDFObjectProperty(ns="http://purl.org/dc/terms/", name="date")
    public String getDateString() {
        return Utils.toISODate(date);
    }
}
