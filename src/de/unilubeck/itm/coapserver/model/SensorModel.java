package de.unilubeck.itm.coapserver.model;

import de.unilubeck.itm.coapserver.Utils;
import de.unilubeck.itm.coapserver.serialization.*;

import java.util.Arrays;
import java.util.Date;

@RDFDatatype
@RDFDatatypeProperties({@RDFDatatypeProperty(property = SensorModel.VAL_TYPE, ns= SensorModel.NS_TYPE, name="type"),
                        @RDFDatatypeProperty(property = SensorModel.VAL_OBSERVED, ns= SensorModel.NS_OBSERVED, name="observedProperty"),
                        @RDFDatatypeProperty(property = SensorModel.VAL_UOM, ns= SensorModel.NS_UOM, name="uomInUse")})
public class SensorModel {

    public static final String PREFIX_ABOUT = "http://spitfire-project.eu/sensor/";

    public static final String NS_TYPE = "http://www.w3.org/2000/01/rdf-schema#";
    public static final String VAL_TYPE = "http://purl.oclc.org/NET/ssnx/ssn#Sensor";

    public static final String NS_OBSERVED = "http://purl.oclc.org/NET/ssnx/ssn#";
    public static final String VAL_OBSERVED = "http://spitfire-project.eu/property/Temperature";

    public static final String NS_UOM = "http://spitfire-project.eu/cc/spitfireCC_n3.owl#";
    public static final String VAL_UOM = "http://spitfire-project.eu/uom/Centigrade";


    protected Date date;
    protected float[] data;
    protected String ressource_id = "";

    public SensorModel(String ressource_id) {
        this.ressource_id = ressource_id;
    }

    @RDFId
    public String getId() {
        return PREFIX_ABOUT + ressource_id;
    }


    @RDFObjectProperty(ns="http://www.loa-cnr.it/ontologies/DUL.owl#", name="hasValue")
    public String getHasValue() {
        return Arrays.toString(data);
    }

    @RDFObjectProperty(ns="http://purl.org/dc/terms/", name="date")
    public String getDateString() {
        return Utils.toISODate(date);
    }

    public float[] getData() {
        return data;
    }

    public void setData(float[] data) {
        this.data = data;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "{" +
                "data=" + Arrays.toString(data) +
                ",date=" + Utils.toISODate(date) +
                '}';
    }
}
