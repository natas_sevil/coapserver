package de.unilubeck.itm.coapserver.model;

import de.unilubeck.itm.coapserver.serialization.*;

@RDFDatatype
@RDFDatatypeProperties({@RDFDatatypeProperty(property = TimeModel.VAL_TYPE, ns= TimeModel.NS_TYPE, name="type"),
                        @RDFDatatypeProperty(property = TimeModel.VAL_OBSERVED, ns= TimeModel.NS_OBSERVED, name="observedProperty"),
                        @RDFDatatypeProperty(property = TimeModel.VAL_UOM, ns= TimeModel.NS_UOM, name="uomInUse")})
public class TimeModel {

    public static final String PREFIX_ABOUT = "http://spitfire-project.eu/sensor/";

    public static final String NS_TYPE = "http://www.w3.org/2000/01/rdf-schema#";
    public static final String VAL_TYPE = "http://purl.oclc.org/NET/ssnx/ssn#Sensor";

    public static final String NS_OBSERVED = "http://purl.oclc.org/NET/ssnx/ssn#";
    public static final String VAL_OBSERVED = "http://purl.oclc.org/NET/muo/ucum/physical-quality/timeSinceEpoch";

    public static final String NS_UOM = "http://spitfire-project.eu/cc/spitfireCC_n3.owl#";
    public static final String VAL_UOM = "http://spitfire-project.eu/uom/second";

    private String ressource_id;
    protected long data;

    public TimeModel(String ressource_id) {
        this.ressource_id = ressource_id;
    }

    @RDFId
    public String getId() {
        return PREFIX_ABOUT + ressource_id;
    }


    @RDFObjectProperty(ns="http://www.loa-cnr.it/ontologies/DUL.owl#", name="hasValue")
    public String getHasValue() {
        return String.valueOf(data * 1000);
    }


    public long getData() {
        return data;
    }

    public void setData(long data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "{" +
                "time=" + data +
                '}';
    }
}
