package de.unilubeck.itm.coapserver.model;

import de.unilubeck.itm.coapserver.Utils;
import de.unilubeck.itm.coapserver.serialization.*;

import java.util.Arrays;

@RDFDatatype
@RDFDatatypeProperties({@RDFDatatypeProperty(property = PressureSensorModel.VAL_TYPE, ns= PressureSensorModel.NS_TYPE, name="type"),
                        @RDFDatatypeProperty(property = PressureSensorModel.VAL_OBSERVED, ns= PressureSensorModel.NS_OBSERVED, name="observedProperty"),
                        @RDFDatatypeProperty(property = PressureSensorModel.VAL_UOM, ns= PressureSensorModel.NS_UOM, name="uomInUse")})
public class PressureSensorModel extends SensorModel {

    public static final String PREFIX_ABOUT = "http://spitfire-project.eu/sensor/";

    public static final String NS_TYPE = "http://www.w3.org/2000/01/rdf-schema#";
    public static final String VAL_TYPE = "http://purl.oclc.org/NET/ssnx/ssn#Sensor";

    public static final String NS_OBSERVED = "http://purl.oclc.org/NET/ssnx/ssn#";
    public static final String VAL_OBSERVED = "http://purl.oclc.org/NET/muo/ucum/physical-quality/pressure";

    public static final String NS_UOM = "http://spitfire-project.eu/cc/spitfireCC_n3.owl#";
    public static final String VAL_UOM = "http://spitfire-project.eu/uom/pascal";


    public PressureSensorModel(String ressource_id) {
        super(ressource_id);
    }

    @RDFId
    public String getId() {
        return PREFIX_ABOUT + ressource_id;
    }


    @RDFObjectProperty(ns="http://www.loa-cnr.it/ontologies/DUL.owl#", name="hasValue")
    public String getHasValue() {
        //The sensors uom is actually Hektopascal
        //but the RDF Value is in Pascal
        float[] vals = new float[data.length];
        for(int i=0;i<data.length;i++) {
            vals[i] = data[i] * 100;
        }
        return Arrays.toString(vals);
    }

    @RDFObjectProperty(ns="http://purl.org/dc/terms/", name="date")
    public String getDateString() {
        return Utils.toISODate(date);
    }
}
