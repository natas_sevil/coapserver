package de.unilubeck.itm.coapserver.model;

import de.unilubeck.itm.coapserver.Utils;
import de.unilubeck.itm.coapserver.serialization.*;
import java.util.Date;

@RDFDatatype
@RDFDatatypeProperties({@RDFDatatypeProperty(property = LocationModel.VAL_TYPE, ns= LocationModel.NS_TYPE, name="type"),
                        @RDFDatatypeProperty(property = LocationModel.VAL_OBSERVED, ns= LocationModel.NS_OBSERVED, name="observedProperty"),
                        @RDFDatatypeProperty(property = LocationModel.VAL_UOM, ns= LocationModel.NS_UOM, name="uomInUse")})
public class LocationModel {

    public static final String PREFIX_ABOUT = "http://spitfire-project.eu/sensor/";

    public static final String NS_TYPE = "http://www.w3.org/2000/01/rdf-schema#";
    public static final String VAL_TYPE = "http://purl.oclc.org/NET/ssnx/ssn#Sensor";

    public static final String NS_OBSERVED = "http://purl.oclc.org/NET/ssnx/ssn#";
    public static final String VAL_OBSERVED = "http://purl.oclc.org/NET/muo/ucum/physical-quality/coordinates";

    public static final String NS_UOM = "http://spitfire-project.eu/cc/spitfireCC_n3.owl#";
    public static final String VAL_UOM = "http://spitfire-project.eu/uom/degree";

    protected Date date;

    protected double longitude = 0;
    protected double lattitude = 0;

    protected String ressource_id = "";

    public LocationModel(String ressource_id) {
        this.ressource_id = ressource_id;
    }

    @RDFId
    public String getId() {
        return PREFIX_ABOUT + ressource_id;
    }

    @RDFObjectProperty(ns="http://www.loa-cnr.it/ontologies/DUL.owl#", name="longitude")
    public String getLongitudeValue() {
        return String.valueOf(longitude);
    }

    @RDFObjectProperty(ns="http://www.loa-cnr.it/ontologies/DUL.owl#", name="lattitude")
    public String getLattitudeValue() {
        return String.valueOf(lattitude);
    }



    @RDFObjectProperty(ns="http://purl.org/dc/terms/", name="date")
    public String getDateString() {
        return Utils.toISODate(date);
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLattitude() {
        return lattitude;
    }

    public void setLatitude(double lattitude) {
        this.lattitude = lattitude;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "{" +
                "lattitude=" + lattitude +
                ",longitude=" + longitude +
                ",date=" + Utils.toISODate(date) +
                '}';
    }
}
