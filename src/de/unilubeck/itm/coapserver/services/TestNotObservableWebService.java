package de.unilubeck.itm.coapserver.services;

import de.uniluebeck.itm.spitfire.nCoap.application.webservice.NotObservableWebService;
import de.uniluebeck.itm.spitfire.nCoap.message.CoapRequest;
import de.uniluebeck.itm.spitfire.nCoap.message.CoapResponse;
import de.uniluebeck.itm.spitfire.nCoap.message.MessageDoesNotAllowPayloadException;
import de.uniluebeck.itm.spitfire.nCoap.message.header.Code;

import java.net.InetSocketAddress;

public class TestNotObservableWebService extends NotObservableWebService {
    public TestNotObservableWebService(String path) {
        super(path, true);
    }

    @Override
    public CoapResponse processMessage(CoapRequest request, InetSocketAddress remoteAddress) {
        CoapResponse response = new CoapResponse(Code.CONTENT_205);

        try {
            response.setPayload("foobar".getBytes());
        } catch (MessageDoesNotAllowPayloadException e) {
            e.printStackTrace();
        }
        return response;
    }
}
