package de.unilubeck.itm.coapserver.services;

import android.util.Log;
import de.uniluebeck.itm.spitfire.nCoap.application.CoapClientApplication;
import de.uniluebeck.itm.spitfire.nCoap.application.CoapServerApplication;
import de.uniluebeck.itm.spitfire.nCoap.application.webservice.WebService;
import de.uniluebeck.itm.spitfire.nCoap.application.webservice.WellKnownCoreResource;
import de.uniluebeck.itm.spitfire.nCoap.message.CoapRequest;
import de.uniluebeck.itm.spitfire.nCoap.message.InvalidMessageException;
import de.uniluebeck.itm.spitfire.nCoap.message.header.Code;
import de.uniluebeck.itm.spitfire.nCoap.message.header.MsgType;
import de.uniluebeck.itm.spitfire.nCoap.message.options.InvalidOptionException;
import de.uniluebeck.itm.spitfire.nCoap.message.options.ToManyOptionsException;
import org.jboss.netty.channel.*;

import java.lang.reflect.Field;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.ConcurrentHashMap;

public class CoapServer extends CoapServerApplication {

    private Channel channel;
    private URI sspURI;

    public CoapServer() {
        super();
    }


    @Override
    public void handleRetransmissionTimout() {
    }

    protected ConcurrentHashMap<String, WebService> getRegisteredServices() {
        try {
            Field privateStringField = getClass().getDeclaredField("registeredServices");
            return (ConcurrentHashMap<String, WebService>) privateStringField.get(this);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return new ConcurrentHashMap<String, WebService>();
    }


    public void sendSSPRegistration() throws InvalidOptionException, InvalidMessageException, ToManyOptionsException, URISyntaxException {
        if(channel != null && sspURI != null) {
            CoapRequest req = new CoapRequest(MsgType.CON, Code.GET,sspURI );
            InetSocketAddress rcptSocketAddress = new InetSocketAddress(req.getTargetUri().getHost(),req.getTargetUri().getPort());
            ChannelFuture future = channel.write(req, rcptSocketAddress);
        }
        else throw new RuntimeException("Channel or URI is null");
    }



    @Override
    public void channelOpen(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        super.channelOpen(ctx, e);
        channel = ctx.getChannel();
    }

    public URI getSspURI() {
        return sspURI;
    }

    public void setSspURI(URI sspURI) {
        this.sspURI = sspURI;
    }
}
