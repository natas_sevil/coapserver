package de.unilubeck.itm.coapserver.services;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import de.unilubeck.itm.coapserver.filters.GZIPPostFilter;
import de.unilubeck.itm.coapserver.filters.PostFilters;
import de.unilubeck.itm.coapserver.model.SensorModel;
import de.unilubeck.itm.coapserver.serialization.RDFUtils;
import de.uniluebeck.itm.spitfire.nCoap.message.CoapRequest;
import de.uniluebeck.itm.spitfire.nCoap.message.CoapResponse;
import de.uniluebeck.itm.spitfire.nCoap.message.header.Code;

import java.net.InetSocketAddress;
import java.util.Calendar;
import java.util.Map;

@PostFilters({GZIPPostFilter.class})
public class SensorService extends BasicObservableWebService<Boolean> {

    private final SensorEventListener listener;
    //private float[] data = new float[0];
    private Sensor sensor;
    private final SensorManager sensorManager;
    private SensorModel sensorModel;

    public SensorService(String path, Context context, int sensorType, int rate, final SensorModel sensorModel) {
        super(path, true);
        this.sensorModel = sensorModel;
        sensorManager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
        this.sensor = sensorManager.getDefaultSensor(sensorType);
        listener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                sensorModel.setData(event.values);
                sensorModel.setDate(Calendar.getInstance().getTime());

                setResourceStatus(true);
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
                return;
            }
        };
        sensorManager.registerListener(listener, sensor, rate);
    }


    @Override
    public CoapResponse processGET(CoapRequest request, InetSocketAddress remoteAddress, Map<String, String> params) throws Exception {
        CoapResponse response = new CoapResponse(Code.CONTENT_205);
        RDFUtils.writeModelResponse(response,sensorModel, params.get("lang"));
        return response;
    }

    @Override
    public CoapResponse processPOST(CoapRequest request, InetSocketAddress remoteAddress, Map<String, String> params) throws Exception {
        if(params.containsKey("rate")) {
            int value = Integer.parseInt(params.get("rate"));
            sensorManager.unregisterListener(listener);
            sensorManager.registerListener(listener, sensor, value);
        }
        return new CoapResponse(Code.VALID_203);
    }

}
