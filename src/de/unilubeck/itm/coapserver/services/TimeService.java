package de.unilubeck.itm.coapserver.services;

import de.unilubeck.itm.coapserver.filters.GZIPPostFilter;
import de.unilubeck.itm.coapserver.filters.PostFilters;
import de.unilubeck.itm.coapserver.model.TimeModel;
import de.unilubeck.itm.coapserver.serialization.RDFUtils;
import de.uniluebeck.itm.spitfire.nCoap.message.CoapRequest;
import de.uniluebeck.itm.spitfire.nCoap.message.CoapResponse;
import de.uniluebeck.itm.spitfire.nCoap.message.header.Code;

import java.net.InetSocketAddress;
import java.util.Map;

@PostFilters({GZIPPostFilter.class})
public class TimeService extends BasicObservableWebService<Boolean> {

    public static final int TIME = 500;
    private long sysTime;
    private TimeModel timeModel;

    public TimeService(String path, TimeModel timeModel) {
        super(path, true);
        this.timeModel = timeModel;
        updateTime.start();
    }


    @Override
    public CoapResponse processGET(CoapRequest request, InetSocketAddress remoteAddress, Map<String, String> params) throws Exception {
        CoapResponse response = new CoapResponse(Code.CONTENT_205);
        RDFUtils.writeModelResponse(response,timeModel, params.get("lang"));
        return response;
    }

    private Thread updateTime = new Thread() {
        @Override
        public void run() {
            while(true) {
                try {
                    sleep(TIME);
                    sysTime = System.currentTimeMillis();
                    timeModel.setData(sysTime);
                    setResourceStatus(true);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    };
}
