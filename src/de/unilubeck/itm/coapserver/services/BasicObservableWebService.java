package de.unilubeck.itm.coapserver.services;

import de.unilubeck.itm.coapserver.Utils;
import de.unilubeck.itm.coapserver.filters.PostFilter;
import de.unilubeck.itm.coapserver.filters.PostFilters;
import de.unilubeck.itm.coapserver.filters.PreFilter;
import de.unilubeck.itm.coapserver.filters.PreFilters;
import de.uniluebeck.itm.spitfire.nCoap.application.webservice.ObservableWebService;
import de.uniluebeck.itm.spitfire.nCoap.message.CoapRequest;
import de.uniluebeck.itm.spitfire.nCoap.message.CoapResponse;
import de.uniluebeck.itm.spitfire.nCoap.message.MessageDoesNotAllowPayloadException;
import de.uniluebeck.itm.spitfire.nCoap.message.header.Code;
import de.uniluebeck.itm.spitfire.nCoap.message.options.Option;
import de.uniluebeck.itm.spitfire.nCoap.message.options.OptionRegistry.MediaType;
import de.uniluebeck.itm.spitfire.nCoap.message.options.OptionRegistry.OptionName;

import java.net.InetSocketAddress;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public abstract class BasicObservableWebService<T> extends ObservableWebService<T> {

    private List<PreFilter> preFilters;
    private List<PostFilter> postFilters;

    public BasicObservableWebService(String path, T initialStatus) {
        super(path, initialStatus);

        preFilters = new LinkedList<PreFilter>();
        PreFilters preAnnotation =  getClass().getAnnotation(PreFilters.class);
        if(preAnnotation != null) {
            Class<? extends PreFilter>[] preFilters = preAnnotation.value();

            for(Class filterClass : preFilters) {
                try {
                    PreFilter filter = (PreFilter) filterClass.newInstance();
                    this.preFilters.add(filter);
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }

        postFilters = new LinkedList<PostFilter>();
        PostFilters postAnnotation = getClass().getAnnotation(PostFilters.class);
        if(postAnnotation != null) {
            Class<? extends PostFilter>[] postFilters = postAnnotation.value();
            for(Class filterClass : postFilters) {
                try {
                    PostFilter filter = (PostFilter) filterClass.newInstance();
                    this.postFilters.add(filter);
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }


    }

    /**
     * Process CoAP message and execute one of processGET, processPUT, processPOST, processDELETE, processDefault
     * depending on request method code.
     * This method wraps exceptions and sends an error response containing exception message
     * @param request
     * @param remoteAddress
     * @return
     */
    public CoapResponse processMessage(CoapRequest request, InetSocketAddress remoteAddress) {
        try {

            preProcess(request);

            Map<String,String> params = Utils.parseParams(request.getOption(OptionName.URI_QUERY));

            List<Option> acceptOptions = request.getOption(OptionName.ACCEPT);
            for(Option o : acceptOptions) {
                String lang = Utils.MEDIA_TYPE_MAP.inverse().get(MediaType.getByNumber((Long) o.getDecodedValue()));
                if(lang != null && !params.containsKey("lang")) {
                    params.put("lang", lang);
                    break;
                }
            }

            CoapResponse response = null;
            if(request.getCode() == Code.GET) {
                response = processGET(request, remoteAddress, params);
            }
            else if(request.getCode() == Code.PUT) {
                response = processPUT(request, remoteAddress, params);
            }
            else if(request.getCode() == Code.POST) {
                response = processPOST(request, remoteAddress, params);
            }
            else if(request.getCode() == Code.DELETE) {
                response = processDELETE(request, remoteAddress, params);
            }
            else {
                response = processDefault(request, remoteAddress, params);
            }
            postProcess(request, response);
            return response;
        }
        catch (Exception e) {
            return sendError(e, Code.INTERNAL_SERVER_ERROR_500);
        }
    }

    private void preProcess(CoapRequest request) throws Exception {
        for(PreFilter filter : preFilters) {
            filter.preProcess(request);
        }
    }

    private void postProcess(CoapRequest request, CoapResponse response) throws Exception {
        for(PostFilter filter : postFilters) {
            filter.postProcess(request, response);
        }
    }


    /**
     * Process GET request. Default implementation sends Code.METHOD_NOT_ALLOWED_405
     *
     * @param request
     * @param remoteAddress
     * @param params
     * @return CoapResponse
     * @throws Exception
     */
    public CoapResponse processGET(CoapRequest request, InetSocketAddress remoteAddress, Map<String, String> params) throws Exception {
        return new CoapResponse(Code.METHOD_NOT_ALLOWED_405);
    }

    /**
     * Process PUT request. Default implementation sends Code.METHOD_NOT_ALLOWED_405
     *
     * @param request
     * @param remoteAddress
     * @param params
     * @return CoapResponse
     * @throws Exception
     */
    public CoapResponse processPUT(CoapRequest request, InetSocketAddress remoteAddress, Map<String, String> params) throws Exception {
        return new CoapResponse(Code.METHOD_NOT_ALLOWED_405);
    }


    /**
     * Process POST request. Default implementation sends Code.METHOD_NOT_ALLOWED_405
     *
     * @param request
     * @param remoteAddress
     * @param params
     * @return CoapResponse
     * @throws Exception
     */
    public CoapResponse processPOST(CoapRequest request, InetSocketAddress remoteAddress, Map<String, String> params) throws Exception {
        return new CoapResponse(Code.METHOD_NOT_ALLOWED_405);
    }

    /**
     * Process DELETE request. Default implementation sends Code.METHOD_NOT_ALLOWED_405
     *
     * @param request
     * @param remoteAddress
     * @param params
     * @return CoapResponse
     * @throws Exception
     */
    public CoapResponse processDELETE(CoapRequest request, InetSocketAddress remoteAddress, Map<String, String> params) throws Exception {
        return new CoapResponse(Code.METHOD_NOT_ALLOWED_405);
    }

    /**
     * Fallthrough if client sends unknown method code. Default implementation sends Code.BAD_REQUEST_400
     *
     * @param request
     * @param remoteAddress
     * @param params
     * @return CoapResponse
     * @throws Exception
     */
    public CoapResponse processDefault(CoapRequest request, InetSocketAddress remoteAddress, Map<String, String> params) throws Exception {
        return new CoapResponse(Code.BAD_REQUEST_400);
    }

    /**
     * Forward exception text to client.
     * @param e
     * @param code Error Code like Code.SERVICE_UNAVAILABLE_503
     * @return
     */
    public CoapResponse sendError(Exception e, Code code) {
        e.printStackTrace();
        CoapResponse response  = new CoapResponse(code);
        try {
            response.setPayload(e.toString().getBytes());
        } catch (MessageDoesNotAllowPayloadException e1) {
            e1.printStackTrace();
        }
        return response;
    }
}
