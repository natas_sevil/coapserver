package de.unilubeck.itm.coapserver.services;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import de.unilubeck.itm.coapserver.filters.GZIPPostFilter;
import de.unilubeck.itm.coapserver.filters.PostFilters;
import de.unilubeck.itm.coapserver.model.LocationModel;
import de.unilubeck.itm.coapserver.serialization.RDFUtils;
import de.uniluebeck.itm.spitfire.nCoap.message.CoapRequest;
import de.uniluebeck.itm.spitfire.nCoap.message.CoapResponse;
import de.uniluebeck.itm.spitfire.nCoap.message.header.Code;

import java.net.InetSocketAddress;
import java.util.Calendar;
import java.util.Map;

@PostFilters({GZIPPostFilter.class})
public class LocationService extends BasicObservableWebService<Boolean> {


    private Location location;
    private LocationModel locationModel;

    public LocationService(String path, Context context, LocationModel locationModel) {
        super(path, true);
        this.locationModel = locationModel;
        final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, false);
        if(provider != null) {
            location = locationManager.getLastKnownLocation(provider);
            locationManager.requestLocationUpdates(provider, 400, 1, new LocationListener() {
                @Override
                public void onLocationChanged(Location loc) {
                    location = loc;
                    setResourceStatus(true);
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {
                    location = locationManager.getLastKnownLocation(provider);
                    setResourceStatus(true);
                }

                @Override
                public void onProviderEnabled(String provider) {
                    location = locationManager.getLastKnownLocation(provider);
                    setResourceStatus(true);
                }

                @Override
                public void onProviderDisabled(String provider) {
                    location = null;
                    setResourceStatus(true);
                }
            });
        }
    }


    @Override
    public CoapResponse processGET(CoapRequest request, InetSocketAddress remoteAddress, Map<String, String> params) throws Exception {
        CoapResponse response = new CoapResponse(Code.CONTENT_205);

        if(location != null) {
            locationModel.setLatitude(location.getLatitude());
            locationModel.setLongitude(location.getLongitude());
            locationModel.setDate(Calendar.getInstance().getTime());
            RDFUtils.writeModelResponse(response,locationModel, params.get("lang"));
        }
        else {
            throw new Exception("Location services not available on this device");
        }
        return response;
    }
}
