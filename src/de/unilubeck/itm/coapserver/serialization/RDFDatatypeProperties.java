package de.unilubeck.itm.coapserver.serialization;

import java.lang.annotation.*;

/**
 * Annotation to allow multiple RDFDatatypeProperty annotations
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface RDFDatatypeProperties {
    public RDFDatatypeProperty[] value();
}
