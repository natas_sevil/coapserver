package de.unilubeck.itm.coapserver.serialization;

import java.lang.annotation.*;

/**
 * Annotation for a RDF Property
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface RDFObjectProperty {
    public String value() default "";
    public String ns();
    public String name() default "";
}
