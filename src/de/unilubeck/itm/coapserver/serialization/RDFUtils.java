package de.unilubeck.itm.coapserver.serialization;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableBiMap;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import de.unilubeck.itm.coapserver.Utils;
import de.uniluebeck.itm.spitfire.nCoap.message.CoapResponse;
import de.uniluebeck.itm.spitfire.nCoap.message.MessageDoesNotAllowPayloadException;
import de.uniluebeck.itm.spitfire.nCoap.message.options.InvalidOptionException;
import de.uniluebeck.itm.spitfire.nCoap.message.options.OptionRegistry.MediaType;
import de.uniluebeck.itm.spitfire.nCoap.message.options.ToManyOptionsException;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.HashMap;

public class RDFUtils {


    private static void addProperty(Model model, Resource resource, Object val, String ns, String fieldName) {
        Property property = model.createProperty(ns, fieldName);

        if(val instanceof Collection) {
            for(Object o : (Collection)val) {
                resource.addProperty(property, o.toString());
            }
        }
        else {
            resource.addProperty(property, val.toString());
        }
    }

    public static Model createModel(Object object) throws IllegalAccessException, InvocationTargetException {
        Model model = ModelFactory.createDefaultModel();
        RDFDatatype rdfModel = object.getClass().getAnnotation(RDFDatatype.class);
        if(rdfModel == null) throw new IllegalStateException("Missing RDFDatatype Annotation on class " + object.getClass().getName());

        /** Get Model ID  **/
        String modelId = null;
        for(Method m : object.getClass().getMethods()) {
            if(m.isAnnotationPresent(RDFId.class)) {
                modelId = m.invoke(object, null).toString();
                break;
            }
        }
        if(modelId == null) throw new IllegalStateException("Missing RDFId Method Annotation in class " + object.getClass().getName());

        Resource resource = model.createResource(modelId);

        /** Handle RDF Ressources */
        RDFDatatypeProperties rdfRessources = object.getClass().getAnnotation(RDFDatatypeProperties.class);

        if(rdfRessources != null) {

            RDFDatatypeProperty[] rdfRessourcesArray = rdfRessources.value();
            for(RDFDatatypeProperty res : rdfRessourcesArray) {
                Resource nestedRessource = model.createResource(res.property());
                Property property = model.createProperty(res.ns(), res.name());
                resource.addProperty(property, nestedRessource);
            }
        }


        /** Handle RDF Properties */
        //Fields
//        for(Field field : object.getClass().getDeclaredFields()) {
//            RDFObjectProperty rdfProperty = field.getAnnotation(RDFObjectProperty.class);
//            if(rdfProperty != null) {
//                field.setAccessible(true);
//                String fieldName = rdfProperty.name().isEmpty() ? field.getName() : rdfProperty.name();
//                Object fieldVal = field.get(object);
//                addProperty(model,resource,fieldVal,rdfProperty.ns(),fieldName);
//            }
//        }

        //Methods
        for(Method method : object.getClass().getDeclaredMethods()) {
            RDFObjectProperty rdfProperty = method.getAnnotation(RDFObjectProperty.class);
            if(rdfProperty != null) {
                method.setAccessible(true);

                String fieldName = method.getName();
                if(!rdfProperty.name().isEmpty()) {
                    fieldName = rdfProperty.name();
                }
                else if(fieldName.startsWith("get")) {
                    fieldName = fieldName.substring(3,4).toLowerCase() +fieldName.substring(0,4);
                }

                addProperty(model,resource, method.invoke(object, null),rdfProperty.ns(),fieldName);
            }
        }

        return model;
    }




    /**
     *
     * @param object @RDFDatatype annotated POJO
     * @param lang "RDF/XML", "RDF/XML-ABBREV", "N-TRIPLE", "TURTLE",  "TTL", "N3", "TEXT", null
     * @return
     */
    public static void writeModelResponse(CoapResponse response, Object object, String lang) throws InvocationTargetException, IllegalAccessException, ToManyOptionsException, InvalidOptionException, MessageDoesNotAllowPayloadException {

        if(lang == null || !(lang.equalsIgnoreCase("RDF/XML") || lang.equalsIgnoreCase("RDF/XML-ABBREV") || lang.equalsIgnoreCase("N-TRIPLE") || lang.equalsIgnoreCase("TURTLE") || lang.equalsIgnoreCase("TTL") || lang.equalsIgnoreCase("N3"))) {
            response.setContentType(MediaType.APP_JSON);
            response.setPayload(object.toString().getBytes());
        }
        else {
            lang = lang.toUpperCase();
            Model model = createModel(object);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            model.write(out, lang);
            response.setContentType(Utils.MEDIA_TYPE_MAP.get(lang));
            response.setPayload(out.toByteArray());
        }
    }
}
