package de.unilubeck.itm.coapserver.serialization;

import java.lang.annotation.*;

/**
 * Annotation to mark a class as a RDFDatatype for serialization
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface RDFDatatype {
    public String value() default "";
}
