package de.unilubeck.itm.coapserver.serialization;

import java.lang.annotation.*;

/**
 * Field or method value will be the id of this model
 *
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface RDFId {
}
