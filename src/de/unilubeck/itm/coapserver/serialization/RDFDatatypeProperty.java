package de.unilubeck.itm.coapserver.serialization;

import java.lang.annotation.*;

/**
 * Annotation for adding a RDFDatatypeProperty to RDFDatatype
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface RDFDatatypeProperty {
    public String property();
    public String ns();
    public String name() default "";
}
