CoAP Test Server

1. Alle URLs unterstützen GET Requests
2. Folgende URLs unterstützen Observation
    /location
    /time
    /sensors/*

3. Die Sensor URLs unter /sensors/* unterstützen POST Requests, um Einstellungen zu verändern.
   Die Parameter müssen x-www-urlencoded sein und können als Query übergeben werden.
   Z.B. /sensors/orientation?rate=3

   Folgende Parameter sind implementiert:
      (i) rate=x, wobei x eine Android Konstante ist. Gültige Werte sind 1,2,3; wobei 1 die schnellste und 3 die normale Update Rate für einen Sensor ist. Default ist 3


